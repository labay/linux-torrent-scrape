# Linux Torrent Scraper

Scrape a bunch of Linux torrents to populate your seedbox

## Introduction

I operate a Linux distro seedbox with ~1200 torrents spanning ~100 Linux distributions. Refreshing all those torrents was becoming a nuisance, so I wrote some code to pull them for me.

Hopefully this project will inspire you to support the FOSS community by running a seedbox of your own.

## Prerequisites

-  Python 3 with:
    -  `requests`
    -  `bs4` (BeautifulSoup)
    -  `progressbar2`

## Usage

### Clone this repo

```
git clone https://gitlab.com/labay/linux-torrent-scrape
```

### Install the prerequisites:

```
pip install -r requirements.txt
```

### Download those torrents!

```
usage: torrentscrape.py [-h] [--list] [--all] [--downloadpath P] [--listonly] [--timeout TIMEOUT] [distros ...]

Scrape Linux torrents for use in populating a seedbox.

positional arguments:
  distros            The distro(s) of choice. --all will scrape all distros.

options:
  -h, --help         show this help message and exit
  --list             List the available distros
  --all              Scrape all available distros.
  --downloadpath P   Path to download the torrents. Defaults to ./torrents
  --listonly         List the torrent URLs without downloading.
  --timeout TIMEOUT  Download timeout, in seconds. Defaults to 10.
```

## Contributing

Please do! Pull requests gladly accepted.

While my Python code (hopefully) isn't complete garbage, clearly I have much to learn - particularly when it comes to condensing repeated processes like pulling a fghjillion torrents. Any (polite) feedback is most appreciated.

## Changelog

### 2023-11-26
-   Correct NetBSD behavior
-   Remove unsupported distro (AgarimOS)
-   Add distros (CachyOS, Liya, Parch, Snal, Vitunix, StormOS, Kumander)
-   Simplify extraction of distros from `distros.py`

### 2023-07-01
-   Add distros (BlendOS, WattOS)

### 2023-04-08
-   Add timeout argument

### 2023-02-19
-   Switch from Selenium to Requests
-   Make `all` option an argument
-   Add `--listonly` argument
-   Improve class structure
-   Create destination directory if it doesn't exist

### 2023-02-11
-   Documentation
-   Progress bars
-   Support for FOSSTorrents pages
-   Add a bunch of new distros, mostly from FOSSTorrents

### 2023-02-09
-   Add a bunch of new distros

### 2023-02-07
-   Switch from gobs of functions to a class with distro-specific attributes