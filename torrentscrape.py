import inspect
import argparse
import requests
import re
import os
from time import sleep
from pathlib import Path
import progressbar

import distros

def ListDistros(DistroDict):
    print('Available distros are:\n' + ' '.join(list(DistroDict.keys())))

def DistroSet(DistroDict, distros, alldistros=False):

    if alldistros:
        ScrapeDistros = list(DistroDict.values())
    elif distros:
        distros = [d.lower() for d in distros]
        ScrapeDistros = [DistroDict[d] for d in DistroDict.keys() if d.lower() in distros]
    else:
        return None

    return ScrapeDistros

def Filename(torrent, filemask):
    e = re.compile(r'[^/]+\.torrent')
    f = re.compile(filemask) if filemask else None
    if filemask:
        try:
            filename = f.search(torrent['text']).group(0) + '.torrent'
        except:
            filename = torrent['text'].replace(' ', '_') + '.torrent'
    else:
        filename = None

    return filename

def Download(torrent, downloadpath, filemask, timeout):
    r = requests.get(torrent['link'], timeout=timeout)
    filename = Filename(torrent, filemask)
    if not filename:
        e = re.compile(r'([^\/]+)$')
        filename = e.search(r.url).group(1)

    with open(downloadpath + '/' + filename, 'wb') as fd:
        fd.write(r.content)

    return None

def Scrape(d, downloadpath='.', sleeptime=0.5, rpad='', listonly=False, timeout=10, mode='a'):
    widgets = [progressbar.FormatLabel(d.name + rpad), progressbar.Bar(), progressbar.SimpleProgress()]
    bar = progressbar.ProgressBar(widgets=widgets, max_value=progressbar.UnknownLength).start()

    torrents = d.Torrents()
    bar = progressbar.ProgressBar(widgets=widgets, max_value=len(torrents)).start()

    n = 0
    for t in torrents:
        f = Filename(t, d.filemask)
        if listonly:
            mode = mode if n == 0 else 'a'
            with open(downloadpath + '/torrents.txt', mode) as fd:
                fd.write(t['link'] + '\n')
            n += 1
            bar.update(n)
        else:
            try:
                Download(t, downloadpath, d.filemask, timeout=timeout)
                n += 1
                bar.update(n)
            except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout, OSError):
                continue
    
    bar.finish()

    return len(torrents), n

def RPad(Distros):
    lengths = [len(d.name) for d in Distros]
    maxlen = max(lengths) + len(str(len(lengths))) * 2 + 4

    rpads = seqs = {}
    for i in range(len(Distros)):
        d = Distros[i]
        seqs[d] = '(' + str(i+1) + '/' + str(len(Distros)) + ')'
        rpads[d] = ' ' + ' '*(maxlen - lengths[i] - len(seqs[d]) - 1) + seqs[d]

    return rpads

def main(args):

    DISTROS = inspect.getmembers(distros, lambda member: inspect.isclass(member) and member.__module__ == 'distros' and len(member.mro()) > 2)
    DistroDict = {}
    for d in DISTROS:
        DistroDict[d[0].lower()] = getattr(distros, d[0].lower())(timeout=args.timeout)

    if args.list or (not args.distros and not args.all):
        ListDistros(DistroDict)

    Path(args.downloadpath).mkdir(parents=True, exist_ok=True)
    downloadpath = Path(args.downloadpath).resolve()
    
    ScrapeDistros = DistroSet(DistroDict, args.distros, alldistros=args.all)
    if ScrapeDistros:
        rpads = RPad(ScrapeDistros)

        for i, d in enumerate(ScrapeDistros):
            try:
                l, n = Scrape(d, downloadpath=str(downloadpath), rpad=rpads[d], listonly=args.listonly, timeout=args.timeout, mode=('w' if i == 0 else 'a'))
            except Exception as e:
                l = n = None

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Scrape Linux torrents for use in populating a seedbox.')
    parser.add_argument('distros', type=str, nargs='*', help='The distro(s) of choice. --all will scrape all distros.')
    parser.add_argument('--list', action='store_true', help='List the available distros')
    parser.add_argument('--all', action='store_true', help='Scrape all available distros.')
    parser.add_argument('--downloadpath', type=str, metavar='P', default='./torrents', help='Path to download the torrents. Defaults to ./torrents')
    parser.add_argument('--listonly', action='store_true', help='List the torrent URLs without downloading.')
    parser.add_argument('--timeout', type=int, default=10, help='Download timeout, in seconds. Defaults to 10.')
    args = parser.parse_args()

    main(args)