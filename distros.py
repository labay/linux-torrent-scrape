import re
import requests
from bs4 import BeautifulSoup
from urllib.parse import urljoin

class Distro:
    def __init__(self, name, torrentsurl, imagesurl=None, torrentre=r'\.torrent$', imagesre=r'\.iso$', filemask=None, timeout=10):
        self.name = name
        self.torrentsurl = torrentsurl
        self.imagesurl = imagesurl
        self.torrentre = torrentre
        self.imagesre = imagesre
        self.filemask = filemask
        self.timeout = timeout

    def Links(self, url=None):
        url = url if url else self.torrentsurl

        def getlinks(url):
            try:    
                html = requests.get(url, timeout=self.timeout).text
            except requests.exceptions.Timeout:
                return []

            soup = BeautifulSoup(html, 'html.parser')
            links = soup.find_all('a')

            hrefs = [str(l.get('href')) for l in links]
            texts = [l.text for l in links]
            
            basedlinks = [urljoin(url, l) for l in hrefs]

            return [{'link': l, 'text': t} for l, t in zip(basedlinks, texts)]

        links = []
        if type(url) is list:
            for u in url:
                links += getlinks(u)
        else:
            links = getlinks(url)

        return links

    def Torrents(self):
        links = self.Links()
        r = re.compile(self.torrentre)

        torrents = [l for l in links if r.search(l['link'])]

        return torrents
    
    def Images(self):
        links = self.Links(self.imagesurl)
        r = re.compile(self.imagesre)

        images = [l for l in links if r.search(l['link'])]

        return images

class FOSSTorrent:
    def __init__(self, name, stub, filemask=None, timeout=10):
        self.name = name
        self.stub = stub
        self.timeout = timeout
        self.filemask = filemask
        self.imagesurl = None

    def filterlinks(self, url, e, base=''):
        try:
            html = requests.get(url, timeout=self.timeout).text
        except requests.exceptions.Timeout:
            return []
        
        soup = BeautifulSoup(html, 'html.parser')
        links = soup.find_all('a')

        hrefs = [base + str(l.get('href')) for l in links]
        texts = [l.text for l in links]

        linkdict = [{'link': l, 'text': t} for l, t in zip(hrefs, texts)]
        linkdict = [l for l in linkdict if e.search(l['link'])]

        return linkdict

    def Torrents(self):
        urlbase = 'https://fosstorrents.com'
        url = urlbase + '/distributions/' + self.stub
        e = re.compile(r'id=\d+(&hybrid=0)*$')
        f = re.compile(r'\.torrent$')

        links = self.filterlinks(url, e, urlbase)

        torrents = []
        for l in links:
            torrents += self.filterlinks(l['link'], f)

        return torrents

class devuan(Distro):
    def __init__(self, **kwargs):
        super().__init__('Devuan', 'https://files.devuan.org/', imagesurl='https://files.devuan.org/devuan_daedalus/installer-iso/', **kwargs)

class arch(Distro):
    def __init__(self, **kwargs):
        super().__init__('Arch', 'https://archlinux.org/download/', torrentre=r'torrent/$', imagesurl='https://ftp.agdsn.de/pub/mirrors/archlinux/iso/2024.01.01/', **kwargs)

class alma(Distro):
    def __init__(self, **kwargs):
        super().__init__('Alma', 'https://mirrors.almalinux.org/isos.html', imagesurl='https://mirrors.almalinux.org/isos.html', **kwargs)

    def Torrents(self):
        editions = self.Links()
        e = re.compile(r'/isos/')
        f = re.compile(r'\.torrent$')

        editions = [ed['link'] for ed in editions if e.search(ed['link'])]

        mirrors = []
        for ed in editions:
            links = self.Links(url=ed)
            mirrorlinks = [l['link'] for l in links if e.search(l['link'])]
            mirrors += [mirrorlinks[0]]

        torrents = []
        for m in mirrors:
            links = self.Links(url=m)
            torrents += [l for l in links if f.search(l['link'])]

        return torrents
    
    def Images(self):
        editions = self.Links()
        e = re.compile(r'/isos/')
        f = re.compile(r'\.iso$')

        editions = [ed['link'] for ed in editions if e.search(ed['link'])]

        mirrors = []
        for ed in editions:
            edmask = re.compile(r'/(\d.*).html$')
            edition = edmask.search(ed).group(1)
            links = self.Links(url=ed)
            links = [l for l in links if '/'+edition+'/' in l['link']]
            mirrorlinks = [l['link'] for l in links if e.search(l['link'])]
            mirrors += [mirrorlinks[0]]
        
        torrents = []
        for m in mirrors:
            links = self.Links(url=m+'/')
            torrents += [l for l in links if f.search(l['link'])]

        return torrents

class antix(Distro):
    def __init__(self, **kwargs):
        super().__init__('AntiX', 'https://antixlinux.com/torrent-files/', imagesurl='https://mirror.its.dal.ca/mxlinux-cd/ANTIX/Final/', torrentre=r'download_id=\d+$', filemask=r'antiX\S+', **kwargs)

    def Images(self):
        url = self.imagesurl
        try:    
            html = requests.get(url, timeout=self.timeout).text
        except requests.exceptions.Timeout:
            return []
        
        soup = BeautifulSoup(html, 'html.parser')
        trs = soup.find_all('tr')
        releases = [r for r in trs if r.find('td', {'class': 'date'}) and re.compile(r'\d{4}').match(r.find('td', {'class': 'date'}).text)]
        from datetime import datetime
        dates = [datetime.strptime(r.find('td', {'class': 'date'}).text, '%Y-%b-%d %H:%M') for r in releases]
        release = releases[dates.index(max(dates))].find('a').text
        releaseurl = self.imagesurl + release

        links = self.Links(releaseurl)
        r = re.compile(self.imagesre)

        images = [l for l in links if r.search(l['link'])]

        return images
    
class armbian(Distro):
    def __init__(self, **kwargs):
        super().__init__('Armbian', 'https://www.armbian.com/download/', imagesurl='https://www.armbian.com/download/', **kwargs)

    def SBCs(self):
        try:
            html = requests.get(self.torrentsurl, timeout=10).text
        except requests.exceptions.Timeout:
            return []

        soup = BeautifulSoup(html, 'html.parser')
        h4s = soup.find_all('h4', class_='pt-cv-title')
        links = []
        for h in h4s:
            links += [l.get('href') for l in h.find_all('a')]
        
        return links

    def Torrents(self):
        sbcs = self.SBCs()

        torrents = []
        for s in sbcs:
            links = self.Links(url=s)
            e = re.compile(r'\.torrent$')
            torrents += [l for l in links if e.search(l['link'])]
        
        return torrents
    
    def Images(self):
        sbcs = self.SBCs()

        images = []
        for s in sbcs:
            links = self.Links(url=s)
            e = re.compile(r'^https*:\/\/dl.armbian.com\/[^\.]+$')
            images += [l for l in links if e.search(l['link'])]

        f = re.compile(r'([^\/]+)$')
        for i in images:
            if f.search(i['link']):
                i['text'] = f.search(i['link']).group(1)
        
        return images

class debian(Distro):
    def __init__(self, **kwargs):
        super().__init__('Debian', 'https://www.debian.org/CD/torrent-cd/', imagesurl='https://www.debian.org/CD/http-ftp/', **kwargs)

    def Torrents(self):
        editions = self.Links()
        e = re.compile(r'/bt-cd/$')
        f = re.compile(r'\.torrent$')

        editions = [ed['link'] for ed in editions if e.search(ed['link'])]

        torrents = []
        for ed in editions:
            links = self.Links(url=ed)
            torrents += [l for l in links if f.search(l['link'])]
            
        return torrents
    
    def Images(self):
        editions = self.Links(self.imagesurl)
        e = re.compile(r'/iso-(cd)*(dvd)*/$')
        f = re.compile(r'\.iso$')

        editions = [ed['link'] for ed in editions if e.search(ed['link'])]

        torrents = []
        for ed in editions:
            links = self.Links(url=ed)
            torrents += [l for l in links if f.search(l['link'])]
            
        return torrents

class endeavour(Distro):
    def __init__(self, **kwargs):
        super().__init__('Endeavour_OS', 'https://endeavouros.com', imagesurl='https://endeavouros.com', **kwargs)

    def Images(self):
        armimagesurl = 'https://endeavouros.com/endeavouros-arm-install/'
        x64links = self.Links(self.imagesurl)
        armlinks = self.Lilnks(armimagesurl)
        r = re.compile(r'\.iso$')
        s = re.compile(r'\.img\.xz$')

        images = [[l for l in x64links if r.search(l['link'])][0]]
        images += [[l for l in armlinks if s.search(s['link'])][0]]

        return images

class fedora(Distro):
    def __init__(self, **kwargs):
        super().__init__('Fedora', 'https://torrent.fedoraproject.org/', imagesurl='https://mirror.pilotfiber.com/fedora/linux/releases/', **kwargs)

    def versions(self):
        e = re.compile(r'(\d+)\/$')
        links = [l for l in self.Links(self.imagesurl) if e.search(l['link'])]
        for l in links:
            l['version'] = int(e.search(l['link']).group(1))
        links = sorted(links, key=lambda l: l['version'], reverse=True)

        return links[:2]
    
    def editions(self):
        e = re.compile(r'(\d+\/)')
        links = self.Links([v['link'] for v in self.versions()])
        return [l for l in links if e.search(l['link']) and l['link'][-1] == '/']
    
    def architectures(self):
        arches = []
        for e in self.editions():
            arches += [l for l in self.Links(e['link']) if len(l['link']) > len(e['link'])]
        return arches
    
    def isos(self):
        isos = []
        e = re.compile(r'\/iso\/$')
        for a in self.architectures():
            isos += [l for l in self.Links(a['link']) if e.search(l['link'])]
        return isos
    
    def Images(self):
        images = []
        e = re.compile(self.imagesre)
        for i in self.isos():
            images += [l for l in self.Links(i['link']) if e.search(l['link'])]
        return images

class garuda(Distro):
    def __init__(self, **kwargs):
        super().__init__('Garuda', 'https://iso.builds.garudalinux.org/iso/latest/garuda/', imagesurl='https://garudalinux.org/downloads.html', imagesre=r'\.iso\?r2\=1$', **kwargs)
    
    def editions(self):
        e = re.compile(r'/garuda/.*/$')
        links = [l for l in self.Links(self.torrentsurl) if e.search(l['link'])]
        return links

    def Torrents(self):
        torrents = []
        for e in self.editions():
            r = re.compile(self.torrentre)
            torrents += [l for l in self.Links(e['link']) if r.search(l['link'])]

        return torrents

class kali(Distro):
    def __init__(self, **kwargs):
        super().__init__('Kali', 'https://www.kali.org/get-kali/', imagesurl='https://www.kali.org/get-kali/', imagesre=r'\.(iso)*(xz)*$', **kwargs)

class kde_neon(Distro):
    def __init__(self, **kwargs):
        super().__init__('KDE_Neon', 'https://neon.kde.org/download', imagesurl='https://neon.kde.org/download', **kwargs)

class linux_lite(Distro):
    def __init__(self, **kwargs):
        super().__init__('Linux_Lite', 'https://www.linuxliteos.com/download.php', **kwargs)

    def Links(self, url=None):
        url = url if url else self.torrentsurl
        try:    
            html = requests.get(url, timeout=self.timeout).text
        except requests.exceptions.Timeout:
            return []

        soup = BeautifulSoup(html, 'html.parser')
        links = soup.find_all('option')

        hrefs = [str(l.get('value')) for l in links]
        t = re.compile(r'([^/]+)/[^/]+$')
        texts = [t.search(h).group(1) if t.search(h) else None for h in hrefs]
        
        basedlinks = [urljoin(url, l) for l in hrefs]

        return [{'link': l, 'text': t} for l, t in zip(basedlinks, texts)]

class mageia(Distro):
    def __init__(self, **kwargs):
        super().__init__('Mageia', 'https://www.mageia.org/en/downloads/alternative/', **kwargs)

    def Editions(self):
        EDITIONS = ['Latest stable release', 'Test release']

        try:
            html = requests.get(self.torrentsurl, timeout=10).text
        except requests.exceptions.Timeout:
            return []

        html = requests.get(self.torrentsurl, timeout=10).text
        soup = BeautifulSoup(html, 'html.parser')

        h3s = soup.find_all('h3')
        editions = [h for h in h3s if h.text in EDITIONS]

        return editions

    def Torrents(self):
        editions = self.Editions()
        
        pages = []
        for e in editions:
            f = re.compile(r'&torrent=1')

            links = e.parent.find_all('a')
            pages += [self.torrentsurl + l.get('href') for l in links if f.search(l.get('href'))]

        torrents = []
        for p in pages:
            links = self.Links(p)
            f = re.compile(self.torrentre)

            torrents += [[l for l in links if f.search(l['link'])][0]]

        return torrents

class manjaro(Distro):
    def __init__(self, **kwargs):
        super().__init__('Manjaro', 'https://manjaro.org/products/download/x86/_payload.json', imagesurl='https://manjaro.org/products/download/x86/_payload.json', filemask=r'.*', **kwargs)

    def JSON(self):
        import json
        try:    
            file = requests.get(self.torrentsurl, timeout=self.timeout).text
            j = json.loads(file)
            k = json.loads(j[3])
            return k
        except requests.exceptions.Timeout:
            return None

    def Torrents(self):
        links = []

        def isrelease(j):
            return type(j) is dict and 'torrent' in j.keys()

        j = self.JSON()
        architectures = list(j.keys())
        for a in architectures:
            arch = j[a]
            editions = list(arch.keys())
            for e in editions:
                edition = arch[e]
                if isrelease(edition):
                    links.append({'link': edition['torrent']})
                else:
                    sbcs = list(edition.keys())
                    for s in sbcs:
                        sbc = edition[s]
                        if isrelease(sbc):
                            links.append({'link': sbc['torrent']})

        e = re.compile("/([^/]+$)")
        for l in links:
            if e.search(l['link']):
                l['text'] = e.search(l['link']).group(1)
        
        return links

    def Images(self):
        links = []

        def isrelease(j):
            return type(j) is dict and 'image' in j.keys()

        j = self.JSON()
        architectures = list(j.keys())
        for a in architectures:
            arch = j[a]
            editions = list(arch.keys())
            for e in editions:
                edition = arch[e]
                if isrelease(edition):
                    links.append({'link': edition['image']})
                else:
                    sbcs = list(edition.keys())
                    for s in sbcs:
                        sbc = edition[s]
                        if isrelease(sbc):
                            links.append({'link': sbc['image']})

        e = re.compile("/([^/]+$)")
        for l in links:
            if e.search(l['link']):
                l['text'] = e.search(l['link']).group(1)
        
        return links

class mint(Distro):
    def __init__(self, **kwargs):
        super().__init__('Linux_Mint', 'https://www.linuxmint.com/download.php', **kwargs)

    def Editions(self):
        try:
            html = requests.get(self.torrentsurl, timeout=10).text
        except requests.exceptions.Timeout:
            return []

        soup = BeautifulSoup(html, 'html.parser')
        links = soup.find_all('a')
        links = [l.get('href') for l in links if l.text == 'Download']
        
        return self.baselinks(links)

    def Torrents(self):
        editions = self.Editions()

        torrents = []
        for e in editions:
            links = self.Links(url=e)
            r = re.compile(self.torrentre)

            torrents += [l for l in links if r.search(l['link'])]
        
        return torrents

class mxlinux(Distro):
    def __init__(self, **kwargs):
        super().__init__('MXLinux', 'https://l2.mxrepo.com/torrents/', **kwargs)

class netbsd(Distro):
    def __init__(self, **kwargs):
        super().__init__('NetBSD', 'https://cdn.netbsd.org/pub/NetBSD/images/', imagesurl='https://cdn.netbsd.org/pub/NetBSD/images/', **kwargs)

    def versions(self):
        e = re.compile(r'(\d+\.\d+)\/$')
        recents = [l for l in self.Links() if e.search(l['link'])]
        recents = sorted(recents, key=lambda l: float(e.search(l['link']).group(1)), reverse=True)
        return recents[:3]
    
    def Torrents(self):
        e = re.compile(self.torrentre)
        torrents = [l for l in self.Links([v['link'] for v in self.versions()]) if e.search(l['link'])]
        return torrents
    
    def Images(self):
        e = re.compile(self.imagesre)
        images = [l for l in self.Links([v['link'] for v in self.versions()]) if e.search(l['link'])]
        return images

class openbsd(Distro):
    def __init__(self, **kwargs):
        super().__init__('OpenBSD', 'http://openbsd.somedomain.net/', **kwargs)

#class peppermint(Distro):
#    def __init__(self, **kwargs):
#        super().__init__('PeppermintOS', 'https://peppermintos.com/guide/downloading/', torrentre=r'torrent/download$', **kwargs)

class peppermint(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('PeppermintOS', 'peppermint-os', **kwargs)

class rocky(Distro):
    def __init__(self, **kwargs):
        super().__init__('Rocky_Linux', 'https://download.rockylinux.org/pub/rocky/', imagesurl='https://download.rockylinux.org/pub/rocky/', imagesre=r'\.iso$', **kwargs)

    def versions(self):
        links = self.Links()
        e = re.compile(r'/(\d+\.*)/$')
        return [l for l in links if e.search(l['link'])]
    
    def releases(self):
        releases = []
        versions = self.versions()
        for v in versions:
            v['link'] += 'isos/'
            links = self.Links(v['link'])
            releases += [l for l in links if len(l['link']) > len(v['link'])]
        
        return releases

    def Torrents(self):
        torrents = []
        releases = self.releases()
        e = re.compile(self.torrentre)
        for r in releases:
            links = self.Links(r['link'])
            torrents += [l for l in links if e.search(l['link'])]
            
        return torrents
    
    def Images(self):
        images = []
        releases = self.releases()
        e = re.compile(self.imagesre)
        for r in releases:
            links = self.Links(r['link'])
            images += [l for l in links if e.search(l['link'])]
            
        return images

class salix(Distro):
    def __init__(self, **kwargs):
        super().__init__('Salix', 'https://www.salixos.org/download.html', **kwargs)

class slackware(Distro):
    def __init__(self, **kwargs):
        super().__init__('Slackware', 'http://www.slackware.com/getslack/torrents.php', **kwargs)

class sparkylinux(Distro):
    def __init__(self, **kwargs):
        super().__init__('SparkyLinux', ['https://sparkylinux.org/download/stable/', 'https://sparkylinux.org/download/rolling/'], **kwargs)

    def Torrents(self, driver=None):
        torrents = []
        for u in self.torrentsurl:
            links = self.Links(url=u)
            r = re.compile(self.torrentre)

            torrents += [l for l in links if r.search(l['link'])]

        return torrents

#class ubuntu(Distro):
#    def __init__(self, **kwargs):
#        super().__init__('Ubuntu', 'https://releases.ubuntu.com/', **kwargs)
#
#    def releases(self):
#        e = re.compile("/(\d+\.*)+/$")
#        releases = [l for l in self.Links() if e.search(l['link'])]
#
#        return releases
#
#    def Torrents(self):
#        torrents = []
#        for r in self.releases():
#            e = re.compile(self.torrentre)
#            links = self.Links(r['link'])
#            torrents += [l for l in links if e.search(l['link'])]
#
#        return torrents
#    
#    def Images(self):
#        images = []
#        for r in self.releases():
#            e = re.compile(self.imagesre)
#            links = self.Links(r['link'])
#            images += [l for l in links if e.search(l['link'])]
#
#        return images

class ubuntu(Distro):
    def __init__(self, **kwargs):
        super().__init__('Ubuntu', 'https://torrent.ubuntu.com/', imagesurl='https://cdimage.ubuntu.com/', **kwargs)

    def variants(self, url=None):
        url = self.torrentsurl if not url else url
        links = [l for l in self.Links(url) if l['link'][-1] == '/']
        for l in links:
            r = requests.get(l['link']+'releases/')
            if r.status_code == 200:
                l['link'] += 'releases/'

        return links

    def releases(self, url=None):
        url = self.torrentsurl if not url else url
        releases = []
        variants = self.variants(url)
        for v in variants:
            releases += [l for l in self.Links(v['link']) if len(l['link']) > len(v['link']) and l['text'][-1] == '/']

        for r in releases:
            r['link'] += 'release/'
        return releases

    def versions(self, url=None):
        url = self.torrentsurl if not url else url
        versions = []
        releases = self.releases(url)
        for r in releases:
            versions += [l for l in self.Links(r['link']) if len(l['link']) > len(r['link']) and l['text'][-1] == '/']

        return versions

    def Torrents(self):
        torrents = []
        versions = self.versions(self.torrentsurl)
        e = re.compile(self.torrentre)
        for v in versions:
            torrents += [l for l in self.Links(v['link']) if e.search(l['link'])]

        return torrents
    
    def Images(self):
        images = []
        releases = self.releases(self.imagesurl)
        e = re.compile(self.imagesre)
        for v in releases:
            images += [l for l in self.Links(v['link']) if e.search(l['link'])]

        return images

class bodhi(Distro):
    def __init__(self, **kwargs):
        super().__init__('Bodhi', 'https://www.bodhilinux.com/download/', **kwargs)

class haiku(Distro):
    def __init__(self, **kwargs):
        super().__init__('Haiku', 'https://www.haiku-os.org/get-haiku/', **kwargs)

class bunsenlabs(Distro):
    def __init__(self, **kwargs):
        super().__init__('BunsenLabs', 'https://www.bunsenlabs.org/installation.html', **kwargs)

class regata(Distro):
    def __init__(self, **kwargs):
        super().__init__('RegataOS', 'https://get.regataos.com.br/p/download.html', **kwargs)

class ghostbsd(Distro):
    def __init__(self, **kwargs):
        super().__init__('GhostBSD', 'https://download.ghostbsd.org/releases/amd64/latest/', **kwargs)

class knoppix(Distro):
    def __init__(self, **kwargs):
        super().__init__('Knoppix', 'http://torrent.unix-ag.uni-kl.de/', **kwargs)

class qubes(Distro):
    def __init__(self, **kwargs):
        super().__init__('Qubes', 'https://www.qubes-os.org/downloads/', **kwargs)

class rosa(Distro):
    def __init__(self, **kwargs):
        super().__init__('ROSA', 'https://mirror.yandex.ru/rosa/rosa2021.1/iso/ROSA.FRESH.12/', imagesurl='https://mirror.yandex.ru/rosa/rosa2021.1/iso/ROSA.FRESH.12/', **kwargs)

    def Releases(self):
        links = self.Links()
        releases = [l['link'] for l in links if self.torrentsurl in l['link']]

        return releases
    
    def Torrents(self):
        links = self.Links(url=self.Releases())
        r = re.compile(self.torrentre)

        torrents = [l for l in links if r.search(l['link'])]

        return torrents
    
    def Images(self):
        links = self.Links(url=self.Releases())
        r = re.compile(self.imagesre)

        images = [l for l in links if r.search(l['link'])]

        return images

class raspberrypi(Distro):
    def __init__(self, **kwargs):
        super().__init__('RaspberryPi_OS', 'https://www.raspberrypi.com/software/operating-systems/', **kwargs)

class endless(Distro):
    def __init__(self, **kwargs):
        super().__init__('EndlessOS', 'https://www.endlessos.org/os-direct-download', **kwargs)

class alterlinux(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('AlterLinux', 'alter-linux', **kwargs)

class archman(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Archman', 'archman', **kwargs)

class arcolinux(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('ArcoLinux', 'arcolinux', **kwargs)

class calinix(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('CalinixOS', 'calinix-os', **kwargs)

class carbonui(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('CarbonUI', 'carbonui', **kwargs)

class hefftor(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Hefftor', 'hefftor', **kwargs)

#class laxer(FOSSTorrent):
#    def __init__(self, **kwargs):
#        super().__init__('LaxerOS', 'laxer-os', **kwargs)

class peux(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('PeuxOS', 'peux-os', **kwargs)

class reborn(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('RebornOS', 'rebornos', **kwargs)

class serene(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Serene_Linux', 'serene-linux', **kwargs)

class xerolinux(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('XeroLinux', 'xerolinux', **kwargs)

class freebsd(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('FreeBSD', 'freebsd', **kwargs)

class hellosystem(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('helloSystem', 'hello', **kwargs)

class midnightbsd(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('MidnightBSD', 'midnightbsd', **kwargs)

class nomadbsd(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('NomadBSD', 'nomad-bsd', **kwargs)

class potabi(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Potabi', 'potabi-systems', **kwargs)

class ravynos(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('ravynOS', 'ravyn-os', **kwargs)

class solobsd(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('SoloBSD', 'solobsd', **kwargs)

class dragonflybsd(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('DragonflyBSD', 'dragonfly-bsd', **kwargs)

class nitrux(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Nitrux', 'nitrux', **kwargs)

class openmediavault(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('OpenMediaVault', 'openmediavault', **kwargs)

class spirallinux(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Spiral_Linux', 'spirallinux', **kwargs)

class amarok(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Amarok', 'amarok-linux', **kwargs)

class canaima(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Canaima', 'canaima', **kwargs)

class dekuve(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('DEKUVE', 'dekuve', **kwargs)

#class finnix(FOSSTorrent):
#    def __init__(self, **kwargs):
#        super().__init__('Finnix', 'finnix', **kwargs)

class grml(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Grml', 'grml', **kwargs)

class calculate(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Calculate', 'calculate-linux', **kwargs)

class porteus_kiosk(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Porteus_Kiosk', 'porteus-kiosk', **kwargs)

class open_indiana(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Open_Indiana', 'open-indiana', **kwargs)

class alpine(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Alpine', 'alpine-linux', **kwargs)

class dahlia(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('dahliaOS', 'dahlia-os', **kwargs)

class easyos(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('EasyOS', 'easy-os', **kwargs)

class gobo(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('GoboLinux', 'gobo-linux', **kwargs)

class kaos(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('KaOS', 'kaos', **kwargs)

class libreelec(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('LibreElec', 'libre-elec', **kwargs)

class minix(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Minix', 'minix', **kwargs)

class pclinuxos(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('PCLinuxOS', 'pc-linux-os', **kwargs)

class puppylinux(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('PuppyLinux', 'puppy-linux', **kwargs)

class reactos(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('ReactOS', 'reactos', **kwargs)

class solus(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Solus', 'solus', **kwargs)

class nutyx(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('NuTyX', 'nutyx', **kwargs)

class centos(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('CentOS', 'centos', **kwargs)

class risios(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('RisiOS', 'risios', **kwargs)

class scientificlinux(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('ScientificLinux', 'scientific-linux', **kwargs)

class ultramarine(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Ultramarine', 'ultramarine-linux', **kwargs)

class blackarch(Distro):
    def __init__(self, **kwargs):
        super().__init__('Blackarch', 'https://blackarch.org/downloads.html', **kwargs)

class kodachi(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Kodachi', 'kodachi', **kwargs)

class athena(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('AthenaOS', 'athena-os', **kwargs)

class parrot(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('ParrotOS', 'parrot-os', **kwargs)

class remnux(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('REMnux', 'remnux', **kwargs)

class robolinux(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('RoboLinux', 'robolinux', **kwargs)

class tails(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Tails', 'tails', **kwargs)

class whonix(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Whonix', 'whonix', **kwargs)

class absolute(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('AbsoluteLinux', 'absolute-linux', **kwargs)

class geckolinux(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('GeckoLinux', 'gecko-linux', **kwargs)

class anvils(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('AnvilsOS', 'anvils-os', **kwargs)

#class caelinux(FOSSTorrent):
#    def __init__(self, **kwargs):
#        super().__init__('CAELinux', 'caelinux', **kwargs)

class drauger(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('DraugerOS', 'drauger-os', **kwargs)

class enso(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('EnsoOS', 'enso-os', **kwargs)

class feren(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('FerenOS', 'feren-os', **kwargs)

class greenie(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('GreenieLinux', 'greenie-linux', **kwargs)

class pop(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Pop!OS', 'pop-os', **kwargs)

class runtu(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Runtu', 'runtu', **kwargs)

class trisquel(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Trisquel', 'trisquel', **kwargs)

class ubuntu_dde(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Ubuntu_DDE', 'ubuntu-dde', **kwargs)

class ubuntu_ed(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Ubuntu_Ed', 'ubuntu-ed', **kwargs)

class ubuntu_kylin(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Ubuntu_Kylin', 'ubuntu-kylin', **kwargs)

class ubuntu_unity(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Ubuntu_Unity', 'ubuntu-unity', **kwargs)

class ubuntu_web(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Ubuntu_Web', 'ubuntu-web', **kwargs)

class ultimate(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('UltimateEdition', 'ultimate-edition', **kwargs)

class voyager(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Voyager', 'voyager', **kwargs)

class zorin(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('ZorinOS', 'zorin-os', **kwargs)

class void_linux(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('VoidLinux', 'void-linux', **kwargs)

class opensuse(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('OpenSUSE', 'opensuse', **kwargs)

class blendos(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('BlendOS', 'blendos', **kwargs)

class wattos(Distro):
    def __init__(self, **kwargs):
        super().__init__('WattOS', 'https://www.planetwatt.com/r13-downloads/', **kwargs)

class cachyos(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('CachyOS', 'cachyos', **kwargs)

class liya(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Liya', 'liya', **kwargs)

class parch(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Parch', 'parch-linux', **kwargs)

class snal(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Snal', 'snal-linux', **kwargs)

class vitunix(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Vitunix', 'vitunix', **kwargs)

class stormos(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('StormOS', 'storm-os', **kwargs)

class kumander(FOSSTorrent):
    def __init__(self, **kwargs):
        super().__init__('Kumander', 'kumander-linux', **kwargs)