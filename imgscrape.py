import inspect
import argparse
import requests
import re
import os
from time import sleep
from pathlib import Path
import progressbar

import distros

def ListDistros(DistroDict):
    print('Available distros are:\n' + ' '.join(list(DistroDict.keys())))

def DistroSet(DistroDict, distros, alldistros=False):

    if alldistros:
        ScrapeDistros = list(DistroDict.values())
    elif distros:
        distros = [d.lower() for d in distros]
        ScrapeDistros = [DistroDict[d] for d in DistroDict.keys() if d.lower() in distros]
    else:
        return None

    return ScrapeDistros

def Filename(distro, torrent):
    e = re.compile(r'[^\/]+' + distro.imagesre)
    if distro.filemask:
        f = re.compile(distro.filemask)
        try:
            filename = f.search(torrent['text']).group(0)
        except:
            filename = torrent['text'].replace(' ', '_') + distro.imagesre[1:-1]
    else:
        filename = None

    return filename

def isAmazon(url):
    a = re.compile(r'(filename%3D)(.*)(&)')
    if a.search(url):
        return True
    return False

def Download(distro, torrent, downloadpath, timeout):
    r = requests.get(torrent['link'], timeout=timeout, stream=True)
    total_size = int(r.headers.get("content-length", 0))
    block_size = 1024
    import numpy as np
    num_bars = np.ceil(total_size / (block_size))
    filename = Filename(distro, torrent)
    if not filename and isAmazon(r.url):
        a = re.compile(r'(filename%3D)(.*)(&)')
        filename = a.search(r.url).group(2)
    elif not filename:
        e = re.compile(r'([^\/]+)$')
        try:
            filename = e.search(r.url).group(1)
        except AttributeError:
            return None

    if os.path.exists(downloadpath + '/' + filename) and os.path.getsize(downloadpath + '/' + filename) == total_size:
        return None

    bar = progressbar.ProgressBar(max_value=num_bars).start()
    with open(downloadpath + '/' + filename, 'wb') as fd:
        for i, chunk in enumerate(r.iter_content(chunk_size=block_size)):
            bar.update(i+1)
            fd.write(chunk)
    bar.finish()

    return None

def Scrape(d, downloadpath='.', sleeptime=0.5, rpad='', listonly=False, timeout=10, mode='a'):
    widgets = [progressbar.FormatLabel(d.name + rpad), progressbar.Bar(), progressbar.SimpleProgress()]
    bar = progressbar.ProgressBar(widgets=widgets, max_value=progressbar.UnknownLength).start()

    images = d.Images()
    bar = progressbar.ProgressBar(widgets=widgets, max_value=len(images)).start()

    n = 0
    for i in images:
        f = Filename(d, i)
        if listonly:
            mode = mode if n == 0 else 'a'
            with open(downloadpath + '/images.txt', mode) as fd:
                    fd.write(i['link'] + '\n')
            n += 1
            bar.update(n)
        else:
            try:
                Download(d, i, downloadpath, timeout=timeout)
                n += 1
                bar.update(n)
            except (requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout, OSError):
                continue
    
    bar.finish()

    return len(images), n

def RPad(Distros):
    lengths = [len(d.name) for d in Distros]
    maxlen = max(lengths) + len(str(len(lengths))) * 2 + 4

    rpads = seqs = {}
    for i in range(len(Distros)):
        d = Distros[i]
        seqs[d] = '(' + str(i+1) + '/' + str(len(Distros)) + ')'
        rpads[d] = ' ' + ' '*(maxlen - lengths[i] - len(seqs[d]) - 1) + seqs[d]

    return rpads

def main(args):

    DISTROS = inspect.getmembers(distros, lambda member: inspect.isclass(member) and member.__module__ == 'distros' and len(member.mro()) > 2)
    DistroDict = {}
    for d in DISTROS:
        D = d[1](timeout=args.timeout)
        if getattr(D, 'imagesurl') is not None:
            DistroDict[d[0].lower()] = D

    if args.list or (not args.distros and not args.all):
        ListDistros(DistroDict)

    Path(args.downloadpath).mkdir(parents=True, exist_ok=True)
    downloadpath = Path(args.downloadpath).resolve()
    
    ScrapeDistros = DistroSet(DistroDict, args.distros, alldistros=args.all)
    if ScrapeDistros:
        rpads = RPad(ScrapeDistros)

        for i, d in enumerate(ScrapeDistros):
            l, n = Scrape(d, downloadpath=str(downloadpath), rpad=rpads[d], listonly=args.listonly, timeout=args.timeout, mode=('w' if i == 0 else 'a'))

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Scrape Linux torrents for use in populating a seedbox.')
    parser.add_argument('distros', type=str, nargs='*', help='The distro(s) of choice. --all will scrape all distros.')
    parser.add_argument('--list', action='store_true', help='List the available distros')
    parser.add_argument('--all', action='store_true', help='Scrape all available distros.')
    parser.add_argument('--downloadpath', type=str, metavar='P', default='./images', help='Path to download the images. Defaults to ./images')
    parser.add_argument('--listonly', action='store_true', help='List the image URLs without downloading.')
    parser.add_argument('--timeout', type=int, default=10, help='Download timeout, in seconds. Defaults to 10.')
    args = parser.parse_args()

    main(args)